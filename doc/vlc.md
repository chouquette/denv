# Run a VideoLAN docker image with your local source code mounted

## Requirements

**NOTE**: I strongly suggest to:
- Do not mount your whole system tree inside the docker container, as it can be harmful
- Mount a "workspace" directory on the same absolute path inside the docker container: this way, paths generated inside the container will work outside (ie `--prefix` will work)
- Configure a [user namespace](./docker_namespace.md) to shift the `root` user in the container to your local `user`
See [this post](https://www.jujens.eu/posts/en/2017/Jul/02/docker-userns-remap/) for more details

## Basic usage

```bash
$ export WORKSPACE=/path/to/your/project/
$ docker run --rm -it -v ${WORKSPACE}:${WORKSPACE} registry.videolan.org/<the_good_docker_image>:latest /bin/bash
```

Replace `<the_good_docker_image>` with:
- **win32** compilation: `vlc-debian-win32`
- **win64** compilation: `vlc-debian-win64`
- **android** compilation: `vlc-debian-android`
- **debian stable** compilation: `vlc-debian-stable`
- **debian unstable** compilation: `vlc-debian-unstable`

## Optional: use the bash source to add the aliases to your shell

This repository provides a script to include in your bash configuration to get some convenient aliases (and less ugly prompts). 

```bash
$ source <this project path>/bash/vlc.alias
```

This will add the `denv compile-vlc-*` aliases to your favorite shell.

## Step-by-step VLC compilation examples

**NOTE**: this part requires that you have followed the previous docker configuration **and** sourced the `bash.sh` file from this project.

### Cross-compiling VLC for Android target

1. Get the VLC sources

    ```bash
    $ cd <workspace>
    $ git clone https://code.videolan.org/videolan/vlc-android vlc-android
    $ cd vlc-android
    ```

2. Build with the docker container

    ```bash
    $ denv compile-vlc-android ./compile.sh
    ```

### Cross-compiling VLC for Windows 64bits target

1. Get the VLC sources

    ```bash
    $ cd <workspace>
    $ git clone http://git.videolan.org/git/vlc.git vlc
    $ cd vlc
    ```

2. Build with the docker container

    ```bash
    $ denv compile-vlc-win64 extras/package/win32/build.sh -l -a x86_64 -i n
    ```

Tip 1: getting an interactive build environment

    ```bash
    $ denv compile-vlc-win64 extras/package/win32/build.sh -s
    ```

Tip 2: use prebuilt contrib

    ```bash
    $ mkdir -p contrib/contrib-win64 && cd contrib/contrib-win64
    $ export URLPREFIX="http://nightlies.videolan.org/build/win64/last-3/vlc-contrib-x86_64-w64-mingw32-" # for vlc 3.X
    $ export URLPREFIX="http://nightlies.videolan.org/build/win64/last/vlc-contrib-x86_64-w64-mingw32-" # for vlc 4.X
    $ wget ${URLPREFIX}`date +%Y%m%d`.tar.bz2 -O vlc-contrib-x86_64-w64-mingw32-latest.tar.bz2 || wget ${URLPREFIX}`date --date=yesterday +%Y%m%d`.tar.bz2 -O vlc-contrib-x86_64-w64-mingw32-latest.tar.bz2
    $ cd ../..
    $ denv compile-vlc-win64 extras/package/win32/build.sh -p -a x86_64 
    ```

### Compiling VLC for Debian stable

1. Get the VLC sources

    ```bash
    $ cd <workspace>
    $ git clone http://git.videolan.org/git/vlc.git vlc
    $ cd vlc
    ```

2. Build with the docker container

    ```bash
    $ denv compile-vlc-stable
    $ ### Bootstrap
    $ ./bootstrap
    $ ### Build extra tools
    $ cd ./extra/tools
    $ ./bootstrap && make -j4
    $ export PATH=$PWD/build/bin:$PATH
    $ cd ../..
    $ ### Build contribs
    $ cd contrib
    $ mkdir native && cd native
    $ export USE_FFMPEG=1
    $ ../bootstrap && make -j4
    $ cd ../..
    $ ### Build VLC
    $ mkdir build && cd build
    $ ../configure
    $ make -j4
    ```

### Compiling VLC for Ubuntu snap

1. Get the VLC sources

    ```bash
    $ cd <workspace>
    $ git clone http://git.videolan.org/git/vlc.git vlc
    $ cd vlc
    ```

2. Put a annonated tag on the commit

    ```bash
    $ git tag -a <my_version_of_snap_package> -m "Some comment"
    ```

This will be used to tag the snap version

3. Build with the docker container

    ```bash
    $ denv compile-vlc-snap
    $ cd extras/package/snap
    $ make -f package.mak snap
    ```

### Further read: VLC Compilation full Documentation

See [the wiki page](https://wiki.videolan.org/Category:Building/)

